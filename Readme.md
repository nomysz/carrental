# CarRental
Recruitment task solution 

Author: nomysz@yahoo.com

### Requirements
- Python 3.7
- pip
- git
- Postgres with default configuration (user `postgres`, no password)
- Heroku CLI

### Setup
- clone repository `git clone https://github.com/nomysz/carrental.git`
- setup and activate venv `cd carrental`, `virtualenv -p {your-python-3.7-path} venv`, `source ./venv/bin/activate`
- install dependencies `pip install -r requirements.txt`
- create database schema `python manage.py migrate`
- create superuser `python manage.py createsuperuser`
- start local server `heroku local web`

### API

##### General information
- token based authorization (but for easy testing browsable API with session based authorization is also enabled)
- local server `http://localhost:5000`

##### Commands

##### Browseable paths

- `/api/cars` CRUD - manage cars
- `/api/reservation` CUD - manage reservation
- `/api/reservation/create` make reservation
- `/api/reservation/cancel` cancel reservation
- `/api/reservation/pdf` generate reservation contract
- `/api/token` token authorization
- `/admin` admin & browseable API login 

##### Browseable API

For your convenience all API enpoints are available in browseable API. It can be removed for production.

### Tests
- run command `python manage.py test -v 2`

### Heroku CLI/deploy/etc related documentation
<https://devcenter.heroku.com/articles/getting-started-with-python>
