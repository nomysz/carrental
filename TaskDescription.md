#### Proszę przygotować prosty projekt Django (JSON API) "wynajem pojazdów", który: 

1. Umożliwia dodawanie/usuwanie samochodów

2. Umożliwia rezerwację pojazdu na dany okres czasu

3. Rezerwacje można odwołać przed rozpoczęciem

4. Dostępna jest historia rezerwacji

5. W trakcie ważności rezerwacji API nie może pozwolić na rejestrację dla kogoś innego

6. Zapytanie o samochód zwraca czy jest teraz dostępny, a jeśli nie, to możemy wiedzieć dla kogo jest zarezerwowany

7. Maksymalny okres jednorazowej rezerwacji to 72h

8. Do samochodu można dodać zdjęcie

9. Użytkownik z flagą is_staff ma dostęp do wszystkich endpointów, pozostali tylko do rezerwacji

10. Stronicowanie wyników dla list

11. Dla każdej rezerwacji można wygenerować umowę w postaci pdf (Lorem ipsum, numer rejestracyjny, okres rezerwacji, miejsce na odręcny podpis)

12. Projekt powinien zawierać testy (przynajmniej podstawowe)

13. Rezygnujemy z rejestracji kont użytkowników

14. Zarządzanie samochodami redukujemy do dodaj/usuń/lista, wymagana jest tylko marka, model, numer rejestracyjny.


Ad 2.
- za jednostkę czasu rezerwacji przyjąłem dzień
- możliwe kierunki rozbudowy to np. wykorzystanie dat ze strefą czasową czy dokładniejsza jednostka czasowa np. godziny
- dodałem warunek uniemozliwiający rezerwację przed obecną datą
  
Ad 3.
- każda rezerwacja otrzymuje unikatowy hash, za pomocą którego można ją anulować oraz pobrać umowę w pdf
- polecenie to jest realizowane metodą POST ze względów bezpieczeństwa podobnie jak inne metody, w których podajemy hash
  (serwer może zapisywać w logach url dla GET)

Ad 4.
- historia rezerwacji zapisywana jest w osobnym modelu, jeśli interesowałaby nas historia wielu modeli lepiej zastosować do tego zewnętrzną bibliotekę
- z poziomu panelu administracyjnego zablokowana jest możliwość edycji historii

Ad 6.
- zapytanie zwraca flagę 'is_available_today' 
- nie zwraca dla kogo jest zarezerwowany gdyż nie trzymamy takiej informacji (brak użytkowników)
- wydaje się też że bardziej odpowiedni byłby endpoint zwracający dostępność dla zadanych parametrów: samochód + zakres dat

Ad 8.
- zdjęcia przez API zwracane są jako url do folderu ze statycznymi plikami
- na potrzeby serwera produkcyjnego serwowanie zdjec powinno odbywac sie z poziomu nginx/apache

Ad 11.
- w przypadku generowania większych dokumentów logikę generowania warto przenieść do osobnej funkcjonalności oraz zapisywać wygenerowane pdf i następnie hostować je z pamięci jeśli już są wygenerowane

Ad 12.
- w kwestii testów zgodnie z opisem zadania pozwoliłem sobie nie skupiać się na pokryciu 100% funkcjonalonści, 
  a bardziej na zaprezentowaniu umiejętności testowania

Inne sugestie poza zadaniem:
- w zaleznosci od potrzeb logika rezerwacji może zostać przeniesiona również do panelu administracyjnego
- aplikację można skonteneryzować, dodać lintery, etc
