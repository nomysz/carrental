from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User, Car, Reservation, ReservationHistory


class UserAdmin(BaseUserAdmin):
    pass


class CarAdmin(admin.ModelAdmin):
    list_display = ('brand', 'model', 'registration_number')


class ReservationAdmin(admin.ModelAdmin):
    list_display = ('car', 'start_date', 'end_date', 'canceled')
    readonly_fields = ('hash',)


class ReservationHistoryAdmin(admin.ModelAdmin):
    list_display = ('car', 'start_date', 'end_date', 'canceled')
    readonly_fields = ('hash', 'car', 'start_date', 'end_date', 'canceled', 'reservation')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(User, UserAdmin)
admin.site.register(Car, CarAdmin)
admin.site.register(Reservation, ReservationAdmin)
admin.site.register(ReservationHistory, ReservationHistoryAdmin)
