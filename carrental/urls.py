from django.urls import path, include
from django.contrib import admin
from django.conf.urls.static import static
from . import settings

admin.autodiscover()

urlpatterns = [
    path('api/', include('api.urls')),
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)  # For development only
