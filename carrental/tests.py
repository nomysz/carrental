from rest_framework.test import APITestCase
from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from carrental import models
import re
from .helpers import generate_random_128_hash


class CarModelTest(TestCase):

    @staticmethod
    def get_test_file():
        return SimpleUploadedFile('test.jpg', b'fake_content')

    def test_car_full_name(self):
        car = models.Car.objects.create(brand='a', model='b', registration_number='c')
        self.assertEqual(car.full_name, 'a b c')

    def test_car_photo(self):
        car = models.Car.objects.create(brand='any', model='any', registration_number='any', photo=self.get_test_file())
        self.assertEqual(self.get_test_file().read(), car.photo.read())


class ReservationModelTest(TestCase):

    def test_reservation_hash_generation(self):
        car1 = models.Car.objects.create(brand='any', model='any', registration_number='any')

        reservation = models.Reservation.objects.create(car=car1,
                                                        start_date=datetime(2050, 10, 10),
                                                        end_date=datetime(2050, 10, 12))

        self.assertTrue(re.match('[a-z0-9]{128}', reservation.hash))

    def test_reservation_collisions(self):
        car1 = models.Car.objects.create(brand='any', model='any', registration_number='any')
        car2 = models.Car.objects.create(brand='any', model='any', registration_number='any')

        models.Reservation.objects.create(car=car1,
                                          start_date=datetime(2050, 10, 10),
                                          end_date=datetime(2050, 10, 12))
        models.Reservation.objects.create(car=car1,
                                          start_date=datetime(2050, 10, 16),
                                          end_date=datetime(2050, 10, 18))

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car2,
                                                                      start_date=datetime(2050, 10, 10),
                                                                      end_date=datetime(2050, 10, 12))),
            0
        )

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car1,
                                                                      start_date=datetime(2050, 10, 13),
                                                                      end_date=datetime(2050, 10, 14))),
            0
        )

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car1,
                                                                      start_date=datetime(2050, 10, 13),
                                                                      end_date=datetime(2050, 10, 15))),
            0
        )

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car1,
                                                                      start_date=datetime(2050, 10, 12),
                                                                      end_date=datetime(2050, 10, 13))),
            1
        )

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car1,
                                                                      start_date=datetime(2050, 10, 12),
                                                                      end_date=datetime(2050, 10, 16))),
            2
        )

    def test_reservation_collisions_with_canceled_reservations(self):
        car1 = models.Car.objects.create(brand='any', model='any', registration_number='any')

        models.Reservation.objects.create(car=car1,
                                          start_date=datetime(2050, 10, 10),
                                          end_date=datetime(2050, 10, 12),
                                          canceled=True)
        models.Reservation.objects.create(car=car1,
                                          start_date=datetime(2050, 10, 12),
                                          end_date=datetime(2050, 10, 13))

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car1,
                                                                      start_date=datetime(2050, 10, 9),
                                                                      end_date=datetime(2050, 10, 10))),
            0
        )

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car1,
                                                                      start_date=datetime(2050, 10, 10),
                                                                      end_date=datetime(2050, 10, 11))),
            0
        )

        self.assertEqual(
            len(models.Reservation.objects.get_colliding_reservations(car=car1,
                                                                      start_date=datetime(2050, 10, 10),
                                                                      end_date=datetime(2050, 10, 12))),
            1
        )

    def test_reservation_history(self):
        car1 = models.Car.objects.create(brand='any', model='any', registration_number='any')
        car2 = models.Car.objects.create(brand='any', model='any', registration_number='any')

        reservation = models.Reservation.objects.create(car=car1,
                                                        start_date=datetime(2050, 10, 10),
                                                        end_date=datetime(2050, 10, 12))

        history = models.ReservationHistory.objects.all()

        self.assertEqual(
            len(history),
            1
        )

        self.assertEqual(
            history[0].car,
            car1
        )

        reservation.car = car2
        reservation.save()

        history_after_save = models.ReservationHistory.objects.all()

        self.assertEqual(
            len(history_after_save),
            2
        )

        self.assertEqual(
            history_after_save[0].car,
            car2
        )

        self.assertEqual(
            history_after_save[1].car,
            car1
        )


class CarsEndpointTest(APITestCase):

    @classmethod
    def create_list_data(cls):
        models.User.objects.create_user(username='test', password='test', email='test@test.com', is_staff=True)

        for i in range(1, 15):
            models.Car.objects.create(model='Test - Model' + str(i), brand='Test - Brand'
                                      + str(i), registration_number='TestNumber - ' + str(i))

    def test_cars_list(self):
        self.create_list_data()
        response = self.client.get('/api/cars/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 10)
        self.assertEqual(type(response.data['results'][0]['id']), int)
        self.assertEqual(response.data['results'][0]['brand'], 'Test - Brand1')
        self.assertEqual(response.data['results'][0]['model'], 'Test - Model1')
        self.assertEqual(response.data['results'][0]['registration_number'], 'TestNumber - 1')
        self.assertEqual(response.data['results'][0]['photo'], None)

    def test_cars_list_pagination(self):
        self.create_list_data()
        response = self.client.get('/api/cars/?page=2')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 4)

    def test_cars_details_as_guest(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        response = self.client.get('/api/cars/{}/'.format(car.id))

        self.assertEqual(response.status_code, 401)

    def test_cars_details_as_staff(self):
        models.User.objects.create_user(username='test', password='test', email='test@test.com', is_staff=True)
        car = models.Car.objects.create(model='Test - Model1', brand='Test - Brand1', registration_number='Number - 1')

        self.client.login(username='test', password='test')

        response = self.client.get('/api/cars/{}/'.format(car.id))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {
            'brand': 'Test - Brand1',
            'model': 'Test - Model1',
            'registration_number': 'Number - 1',
            'photo': None,
            'is_available_today': True
        })

    def test_cars_delete_as_guest(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        response = self.client.get('/api/cars/{}/'.format(car.id))

        self.assertEqual(response.status_code, 401)

    def test_cars_delete_as_staff(self):
        models.User.objects.create_user(username='test', password='test', email='test@test.com', is_staff=True)
        car = models.Car.objects.create(model='Test - Model1', brand='Test - Brand1', registration_number='Number - 1')

        self.client.login(username='test', password='test')

        response = self.client.delete('/api/cars/{}/'.format(car.id))

        self.assertEqual(response.status_code, 204)

        response = self.client.get('/api/cars/{}/'.format(car.id))

        self.assertEqual(response.status_code, 404)

    def test_cars_update_as_guest(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        response = self.client.put('/api/cars/{}/'.format(car.id))

        self.assertEqual(response.status_code, 401)

    def test_cars_update_as_staff(self):
        models.User.objects.create_user(username='test', password='test', email='test@test.com', is_staff=True)
        car = models.Car.objects.create(model='Test - Model1', brand='Test - Brand1', registration_number='Number - 1')

        self.client.login(username='test', password='test')

        response = self.client.put('/api/cars/{}/'.format(car.id), {
            'model': 'model-update-test',
            'brand': 'brand-update-test',
            'registration_number': 'registration_number-update-test',
        })
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/api/cars/{}/'.format(car.id))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {
            'model': 'model-update-test',
            'brand': 'brand-update-test',
            'registration_number': 'registration_number-update-test',
            'photo': None,
            'is_available_today': True
        })


class ReservationsEndpointTest(APITestCase):

    @classmethod
    def prepare_test_data(cls):
        models.User.objects.create_user(username='test', password='test', email='test@test.com', is_staff=True)

        car = models.Car.objects.create(model='any', brand='any', registration_number='any')

        reservation = models.Reservation.objects.create(car=car,
                                                        start_date=datetime(2050, 10, 10),
                                                        end_date=datetime(2050, 10, 12))

        return car.id, reservation

    @classmethod
    def prepare_list_data(cls):
        models.User.objects.create_user(username='test', password='test', email='test@test.com', is_staff=True)

        for i in range(1, 15):
            car = models.Car.objects.create(model='any', brand='any', registration_number='any')
            models.Reservation.objects.create(car=car,
                                              start_date=datetime(2050, 10, 10),
                                              end_date=datetime(2050, 10, 12))

    def test_reservations_list_as_guest(self):
        self.prepare_list_data()
        response = self.client.get('/api/reservations/')

        self.assertEqual(response.status_code, 401)

    def test_reservations_list_as_staff(self):
        self.prepare_list_data()
        self.client.login(username='test', password='test')
        response = self.client.get('/api/reservations/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 10)

        self.assertEqual(type(response.data['results'][0]['id']), int)
        self.assertEqual(type(response.data['results'][0]['start_date']), str)
        self.assertEqual(type(response.data['results'][0]['end_date']), str)
        self.assertEqual(type(response.data['results'][0]['car']), int)
        self.assertTrue(re.match('[a-z0-9]{128}', response.data['results'][0]['hash']))

    def test_reservations_list_pagination(self):
        self.prepare_list_data()
        self.client.login(username='test', password='test')
        response = self.client.get('/api/reservations/?page=2')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 4)

    def test_reservations_any_method_as_guest(self):
        car_id, reservation = self.prepare_test_data()

        response = self.client.get('/api/reservations/')
        self.assertEqual(response.status_code, 401)

        response = self.client.get('/api/reservations/{}/'.format(reservation.id))
        self.assertEqual(response.status_code, 401)

        response = self.client.post('/api/reservations/')
        self.assertEqual(response.status_code, 401)

        response = self.client.delete('/api/reservations/{}/'.format(reservation.id))
        self.assertEqual(response.status_code, 401)

        response = self.client.put('/api/reservations/{}/'.format(reservation.id))
        self.assertEqual(response.status_code, 401)

    def test_reservations_detail_as_staff(self):
        car_id, reservation = self.prepare_test_data()
        self.client.login(username='test', password='test')
        response = self.client.get('/api/reservations/{}/'.format(reservation.id))

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['id'], reservation.id)
        self.assertEqual(response.data['start_date'], '2050-10-10')
        self.assertEqual(response.data['end_date'], '2050-10-12')
        self.assertEqual(response.data['car'], car_id)
        self.assertEqual(response.data['hash'], reservation.hash)

    def test_reservations_update_as_staff(self):
        car_id, reservation = self.prepare_test_data()

        self.client.login(username='test', password='test')
        response = self.client.put('/api/reservations/{}/'.format(reservation.id), {
            'car': car_id,
            'start_date': '2050-10-11',
            'end_date': '2050-10-12',
        })

        self.assertEqual(response.status_code, 200)

        response = self.client.get('/api/reservations/{}/'.format(reservation.id))

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['id'], reservation.id)
        self.assertEqual(response.data['start_date'], '2050-10-11')
        self.assertEqual(response.data['end_date'], '2050-10-12')
        self.assertEqual(response.data['car'], car_id)
        self.assertEqual(response.data['hash'], reservation.hash)

    def test_reservations_delete_as_staff(self):
        car_id, reservation = self.prepare_test_data()

        self.client.login(username='test', password='test')
        response = self.client.delete('/api/reservations/{}/'.format(reservation.id))

        self.assertEqual(response.status_code, 204)

        response = self.client.get('/api/reservations/{}/'.format(reservation.id))

        self.assertEqual(response.status_code, 404)

    def test_reservation_create(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        day_after_tomorrow = date.today() + relativedelta(days=2)

        response = self.client.post('/api/reservations/create', {
            'car': car.id,
            'start_date': date.today(),
            'end_date': day_after_tomorrow,
        })

        self.assertEqual(response.status_code, 201)
        self.assertTrue(re.match('[a-z0-9]{128}', response.data['hash']))

    def test_reservation_create_in_past(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        yesterday = date.today() - relativedelta(days=1)

        response = self.client.post('/api/reservations/create', {
            'car': car.id,
            'start_date': yesterday,
            'end_date': yesterday,
        })

        self.assertEqual(response.status_code, 400)

    def test_reservation_create_with_reversed_dates(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        tomorrow = date.today() + relativedelta(days=1)

        response = self.client.post('/api/reservations/create', {
            'car': car.id,
            'start_date': tomorrow,
            'end_date': date.today(),
        })

        self.assertEqual(response.status_code, 400)

    def test_reservation_create_with_too_long_reservation_period(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        future_date = date.today() + relativedelta(days=3)

        response = self.client.post('/api/reservations/create', {
            'car': car.id,
            'start_date': date.today(),
            'end_date': future_date,
        })

        self.assertEqual(response.status_code, 400)

    def test_reservation_create_with_colliding_reservation(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        future_date = date.today() + relativedelta(days=2)

        models.Reservation.objects.create(car=car,
                                          start_date=future_date,
                                          end_date=future_date)

        response = self.client.post('/api/reservations/create', {
            'car': car.id,
            'start_date': date.today(),
            'end_date': future_date,
        })

        self.assertEqual(response.status_code, 400)

    def test_reservation_cancel_impossible_if_reservation_is_ongoing_or_past(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        yesterday = date.today() - relativedelta(days=1)

        past_reservation = models.Reservation.objects.create(car=car,
                                                             start_date=yesterday,
                                                             end_date=yesterday)

        past_reservation_response = self.client.post('/api/reservations/cancel', {
            'hash': past_reservation.hash
        })

        self.assertEqual(past_reservation_response.status_code, 403)

        today_reservation = models.Reservation.objects.create(car=car,
                                                              start_date=date.today(),
                                                              end_date=date.today())

        today_reservation_response = self.client.post('/api/reservations/cancel', {
            'hash': today_reservation.hash
        })

        self.assertEqual(today_reservation_response.status_code, 403)

    def test_reservation_cancel_possible_if_reservation_is_in_future(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        tomorrow = date.today() + relativedelta(days=1)
        reservation = models.Reservation.objects.create(car=car,
                                                        start_date=tomorrow,
                                                        end_date=tomorrow)

        response = self.client.post('/api/reservations/cancel', {
            'hash': reservation.hash
        })

        self.assertEqual(response.status_code, 200)

    def test_reservation_cancel_with_invalid_hash(self):
        response = self.client.post('/api/reservations/cancel', {
            'hash': generate_random_128_hash()
        })
        self.assertEqual(response.status_code, 401)

    def test_reservation_pdf_generation_with_invalid_hash(self):
        response = self.client.post('/api/reservations/pdf', {
            'hash': generate_random_128_hash()
        })
        self.assertEqual(response.status_code, 403)

    def test_reservation_pdf_generation_for_canceled_reservation(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        tomorrow = date.today() + relativedelta(days=1)
        reservation = models.Reservation.objects.create(car=car,
                                                        start_date=tomorrow,
                                                        end_date=tomorrow,
                                                        canceled=True)

        response = self.client.post('/api/reservations/pdf', {
            'hash': reservation.hash
        })
        self.assertEqual(response.status_code, 403)

    def test_reservation_pdf_generation(self):
        car = models.Car.objects.create(model='any', brand='any', registration_number='any')
        tomorrow = date.today() + relativedelta(days=1)
        reservation = models.Reservation.objects.create(car=car,
                                                        start_date=tomorrow,
                                                        end_date=tomorrow)

        response = self.client.post('/api/reservations/pdf', {
            'hash': reservation.hash
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get('content-type'), 'application/pdf')
        self.assertEqual(response.get('content-disposition'), 'attachment; filename="carrental-reservation.pdf"')
