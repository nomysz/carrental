from django.db import models
from django.contrib.auth.models import AbstractUser
from .helpers import generate_random_128_hash


class User(AbstractUser):
    pass


class Car(models.Model):
    brand = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    registration_number = models.CharField(max_length=50)
    photo = models.FileField(max_length=100, null=True, blank=True, upload_to='car_photos/')

    @property
    def full_name(self):
        return '{} {} {}'.format(self.brand, self.model, self.registration_number)

    def __str__(self):
        return self.full_name


class ReservationManager(models.Manager):

    def get_colliding_reservations(self, car, start_date, end_date):
        return super().get_queryset().filter(car=car)\
                   .filter(canceled=False)\
                   .filter(
                       models.Q(start_date__lte=start_date, end_date__gte=start_date) |
                       models.Q(start_date__lte=end_date, end_date__gte=end_date)
                   )


class AbstractReservation(models.Model):

    class Meta:
        abstract = True

    start_date = models.DateField()
    end_date = models.DateField()
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    hash = models.CharField(unique=True, default=generate_random_128_hash, max_length=128)
    canceled = models.BooleanField(default=False)

    objects = ReservationManager()

    def __str__(self):
        return 'Reservation ' + self.car.full_name


class Reservation(AbstractReservation):

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(Reservation, self).save()

        ReservationHistory.objects.create(
            reservation=self,
            start_date=self.start_date,
            end_date=self.end_date,
            car=self.car,
            hash=self.hash,
            canceled=self.canceled,
        )


class ReservationHistory(AbstractReservation):

    class Meta:
        ordering = ['-pk']

    hash = models.CharField(unique=False, default=generate_random_128_hash, max_length=128)
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE)
