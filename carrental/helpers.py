from django.utils.crypto import get_random_string


def generate_random_128_hash():
    return get_random_string(128).lower()
