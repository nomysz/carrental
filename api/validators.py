from django.core.validators import RegexValidator


alphanumeric_validator = RegexValidator(r'^[0-9a-z]*$', 'Only small letters and numbers are allowed.')
