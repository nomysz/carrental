from api.views import CarViewSet, ReservationViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('cars', CarViewSet)
router.register('reservations', ReservationViewSet)
