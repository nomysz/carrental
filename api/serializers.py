from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied, ValidationError
from carrental.models import Car, Reservation
from dateutil.relativedelta import relativedelta
from datetime import date
from .validators import alphanumeric_validator


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('id', 'brand', 'model', 'registration_number', 'photo')


class CarDetailSerializer(serializers.ModelSerializer):
    is_available_today = serializers.SerializerMethodField()

    class Meta:
        model = Car
        fields = ('brand', 'model', 'registration_number', 'photo', 'is_available_today')

    @staticmethod
    def get_is_available_today(obj):
        colliding_reservations = Reservation.objects.get_colliding_reservations(car=obj,
                                                                                start_date=date.today(),
                                                                                end_date=date.today())

        return False if colliding_reservations else True


class ReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = ('id', 'start_date', 'end_date', 'car', 'hash')


class CreateReservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = ('hash', 'car', 'start_date', 'end_date')
        read_only_fields = ('hash',)
        extra_kwargs = {
            'start_date': {'write_only': True},
            'car': {'write_only': True},
            'end_date': {'write_only': True},
        }

    def validate(self, data):
        start_date, end_date, car = data.get('start_date'), data.get('end_date'), data.get('car')

        if start_date < date.today():
            raise ValidationError('Invalid date range, start_date should be today or in future')

        if start_date > end_date:
            raise ValidationError('Invalid date range, start_date should be earlier than end date')

        max_end_date = start_date + relativedelta(days=3)

        if end_date >= max_end_date:
            raise ValidationError('Invalid date range, reservations allowed to 72 hours max')

        colliding_reservations = Reservation.objects.get_colliding_reservations(car=car,
                                                                                start_date=start_date,
                                                                                end_date=end_date)

        if colliding_reservations:
            raise ValidationError('Invalid date range, car isn\'t available at given dates')

        return data

    def create(self, validated_data):
        reservation = Reservation(**validated_data)
        reservation.save()
        return reservation


class CancelReservationSerializer(serializers.Serializer):

    hash = serializers.CharField(min_length=128, max_length=128, validators=[alphanumeric_validator], write_only=True)

    def create(self, validated_data):
        reservation = Reservation.objects.filter(hash=validated_data['hash']).first()

        if not reservation:
            raise AuthenticationFailed

        if reservation.start_date <= date.today():
            raise PermissionDenied

        reservation.canceled = True
        reservation.save()

        return reservation


class GetReservationPdfSerializer(serializers.Serializer):

    hash = serializers.CharField(min_length=128, max_length=128, validators=[alphanumeric_validator], write_only=True)

    def validate(self, data):
        reservation = Reservation.objects.filter(hash=data.get('hash'), canceled=False)

        if not reservation:
            raise PermissionDenied

        return data
