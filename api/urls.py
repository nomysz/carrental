from rest_framework.authtoken import views as rest_framework_views
from django.urls import path, include
from .router import router
from .views import CreateReservationView, CancelReservationView, GetReservationPdfView


urlpatterns = [
    path('token', rest_framework_views.obtain_auth_token),
    path('reservations/create', CreateReservationView.as_view()),
    path('reservations/cancel', CancelReservationView.as_view()),
    path('reservations/pdf', GetReservationPdfView.as_view()),
    path('', include(router.urls)),
]
