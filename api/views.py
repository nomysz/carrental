from io import BytesIO
from rest_framework import viewsets, mixins, generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser, AllowAny
from django.http import HttpResponse
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen.canvas import Canvas
from carrental.models import Car, Reservation
from .serializers import CarSerializer, ReservationSerializer, CreateReservationSerializer,\
                         CancelReservationSerializer, GetReservationPdfSerializer, CarDetailSerializer


class CarViewSet(viewsets.ModelViewSet):

    queryset = Car.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return CarDetailSerializer
        return CarSerializer

    def get_permissions(self):
        if self.action != 'list':
            return [IsAdminUser()]
        return []


class ReservationViewSet(mixins.RetrieveModelMixin,
                         mixins.UpdateModelMixin,
                         mixins.DestroyModelMixin,
                         mixins.ListModelMixin,
                         viewsets.GenericViewSet):

    permission_classes = (IsAdminUser,)
    serializer_class = ReservationSerializer
    queryset = Reservation.objects.all()


class CreateReservationView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CreateReservationSerializer


class CancelReservationView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CancelReservationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return Response(status=status.HTTP_200_OK)


class GetReservationPdfView(generics.CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = GetReservationPdfSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        reservation = Reservation.objects.filter(hash=request.data.get('hash')).first()

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="carrental-reservation.pdf"'

        buffer = BytesIO()

        pdf = Canvas(buffer, pagesize=letter)

        pdf.setFont('Helvetica', 16)
        pdf.drawString(275, 650, 'CONTRACT')

        pdf.setFont('Helvetica', 12)
        pdf.drawString(30, 550, 'Car: {}'.format(reservation.car.full_name))
        pdf.drawString(30, 530, 'Reservation time: {} - {}'.format(
            reservation.start_date.strftime('%Y/%m/%d'),
            reservation.end_date.strftime('%Y/%m/%d')
        ))

        pdf.setFont('Helvetica', 10)
        pdf.drawString(440, 400, 'Customer\'s signature')
        pdf.setLineWidth(.4)
        pdf.line(420, 420, 555, 420)

        pdf.showPage()
        pdf.save()

        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)

        return response
